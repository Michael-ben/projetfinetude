/* eslint-disable @typescript-eslint/camelcase */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable prefer-const */
import { Injectable, HttpException, HttpStatus, UnauthorizedException} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { LoginDTO} from '../users/dto/login-user.dto';
import { UsersService } from '../users/service/users.service';
import { JwtPayload } from './interfaces/jwt-payload.interface';
import * as bcrypt from 'bcrypt'; 
@Injectable()
export class AuthService {
  constructor(
    private UsersService: UsersService,
    private jwtService: JwtService,
  ) { }
  
  async validateUserByPassword(loginAttempt: LoginDTO) {
    // This will be used for the initial login
    ;
    const { password } = loginAttempt;
    let userToAttempt = await this.UsersService.findOneByUsername(loginAttempt.username);
    let entite = await this.UsersService.findOneByEntite(loginAttempt.entite);
    if (!entite) {
      if (!userToAttempt) {
        throw new HttpException('il y une ou des informations incorrectes', HttpStatus.UNAUTHORIZED)
      }
      else {
        throw new HttpException('entite invalide', HttpStatus.UNAUTHORIZED)
      }
    }
    if (!userToAttempt) {
      throw new HttpException('username invalide', HttpStatus.UNAUTHORIZED)
    }
    if (await bcrypt.compare(password, userToAttempt.password)) {
      
    
      return new Promise((resolve) => {

            resolve(this.createJwtPayload(userToAttempt));

      });
    }
    else {
      throw new HttpException('password invalide', HttpStatus.UNAUTHORIZED)
    }
  }
  async validateUserByJwt(payload: JwtPayload) {
    let user = await this.UsersService.findOneByUsername(payload.username);
      if (user){
        return this.createJwtPayload(user);
    } else {
        throw new UnauthorizedException();;
    }
  }

  createJwtPayload(user) {
    let data: JwtPayload = {
      username: user.username,
      
    };

    let jwt = this.jwtService.sign(data);

    return {
      username: user.username,
      expiresIn: 36000,
      token: jwt,
    };
  }
}
