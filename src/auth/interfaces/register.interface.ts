/* eslint-disable @typescript-eslint/class-name-casing */
import { Document } from 'mongoose';
export interface registerparticulier extends Document{
  checkPassword(password: string, arg1: (err: any, isMatch: any) => void);
  readonly nom: string;
  readonly prenoms: string;
  readonly e_mail: string;
  readonly username: string;
  readonly ville: string;
  readonly entite: string;
  readonly numero_tel: number;
  readonly pays: string
  readonly password: string;


}
export interface registerentreprise extends Document {
  checkPassword(password: string, arg1: (err: any, isMatch: any) => void);
  readonly nom: string;
  readonly prenoms: string;
  readonly e_mail: string;
  readonly raison_social: string;
  readonly ville: string;
  readonly entite: string;
  readonly numero_tel: string;
  readonly pays: string;
  readonly password: string;

}