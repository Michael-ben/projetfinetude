/* eslint-disable @typescript-eslint/no-unused-vars */
import { Controller, Post, Body } from '@nestjs/common';
import { AuthService } from './auth.service';
import { LoginDTO } from '../users/dto/login-user.dto';
@Controller('auth')
export class AuthController {
  constructor(
    private authService: AuthService,
  ) {}
  
  @Post('login')
  async login(@Body() userDto: LoginDTO) {
    return await this.authService.validateUserByPassword(userDto);
  }

}
