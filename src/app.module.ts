import { Module } from '@nestjs/common';
import { MulterModule } from '@nestjs/platform-express';
import { UsersController } from './users/controller/users.controller';
import { AccesController } from './users/controller/Acces.controller';
import { ApplicationController } from './users/controller/application.controller';
import { EntiteController } from './users/controller/Entite.controller';
import { FichierController } from './users/controller/Fichier.controller';
import { ProfilController } from './users/controller/profil.controller';
import { UsersService } from './users/service/users.service';
import { AccesService } from './users/service/Acces.service';
import { ApplicationService } from './users/service/application.service';
import { EntiteService } from './users/service/Entite.service';
import { FichierService } from './users/service/Fichier.service';
import { ProfilService } from './users/service/profil.service';
import { AuthModule } from './auth/auth.module';
import { MongooseModule } from '@nestjs/mongoose';
import { forwardRef } from '@nestjs/common';
import { UserSchema } from './users/schemas/user.schema';
import { ApplicationSchema } from './users/schemas/application.schema';
import { AccesSchema } from './users/schemas/Acces.schema';
import { ProfilSchema } from './users/schemas/profil.schema';
import { EntiteSchema } from './users/schemas/Entite.schema';
import { FichierSchema } from './users/schemas/Fichier.schema';
import { PassportModule } from '@nestjs/passport';
@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'User', schema: UserSchema }]),
    MongooseModule.forFeature([{ name: 'Acces', schema: AccesSchema }]),
    MongooseModule.forFeature([{ name: 'Profil', schema: ProfilSchema }]),
    MongooseModule.forFeature([{ name: 'Fichier', schema: FichierSchema }]),
    MongooseModule.forFeature([{ name: 'Entite', schema: EntiteSchema }]),
    PassportModule.register({ defaultStrategy: 'jwt', session: false }),
    MongooseModule.forFeature([
      { name: 'Application', schema: ApplicationSchema },
    ]),
    MongooseModule.forRoot('mongodb://localhost:27017/akilgeds', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex:true,
    }),
    forwardRef(() => AuthModule),
    MulterModule.register({dest:'./uploads'}) 
  ],
  controllers: [
    UsersController,
    AccesController,
    ProfilController,
    FichierController,
    EntiteController,
    ApplicationController,
  ],
  providers: [
    UsersService,
    AccesService,
    ProfilService,
    FichierService,
    EntiteService,
    ApplicationService,
  ],
  exports: [
    UsersService,
    AccesService,
    ProfilService,
    FichierService,
    EntiteService,
    ApplicationService,
  ],
})
export class AppModule {}
