import { Document } from 'mongoose';
// eslint-disable-next-line @typescript-eslint/class-name-casing
export interface application extends Document {
  readonly nom_app: string;
  readonly created_at: Date;
  readonly updated_at: Date;
  readonly created_by: string;
  readonly uptdated_by: string;
}
