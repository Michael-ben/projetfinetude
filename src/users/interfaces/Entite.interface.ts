import { Document } from 'mongoose';
export interface Entite extends Document {
  checkPassword(password: string, arg1: (err: any, isMatch: any) => any);
  readonly domaine: string;
  readonly created_at: Date;
  readonly updated_at: Date;
  readonly created_by: string;
  readonly uptdated_by: string;
}
