import { Document } from 'mongoose';
export interface Acces extends Document {
  readonly nom_Acces: string;
  readonly created_at: Date;
  readonly updated_at: Date;
  readonly created_by: string;
  readonly uptdated_by: string;
}
