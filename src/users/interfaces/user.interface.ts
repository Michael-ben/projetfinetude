/* eslint-disable @typescript-eslint/camelcase */
import { Document } from 'mongoose';
// eslint-disable-next-line @typescript-eslint/class-name-casing
export interface User extends Document {
  checkPassword(password: string, arg1: (err: any, isMatch: any) => void);
 readonly nom: string;
 readonly prenom: string;
 readonly e_mail: string;
 readonly raison_sociale: string;
 readonly username:string;
  readonly ville: string;
  readonly updated_by: string;
 readonly entite: string;
 readonly numero_tel: number,
 readonly pays: string;
 password: string;
  
  

}
