import { Controller, Body, Post, Res, Get, HttpStatus, Param, NotFoundException, Delete, Query, Put } from '@nestjs/common';
import { CreateEntiteDTO } from '../dto/create-Entite.dto';
import { ValidateObjectId } from '../shared/pipes/validate-object-id.pipes';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { EntiteService } from '../service/Entite.service'

@Controller('entite')
export class EntiteController {
  constructor(private EntiteService: EntiteService) {}
  @Post('/create')
  async createPost(@Res() res, @Body() createEntiteDTO: CreateEntiteDTO) {
    const Entite = await this.EntiteService.createEntites(createEntiteDTO);
    return res.status(HttpStatus.OK).json({
      message: 'Entite créer avec succes',
      Entite: Entite,
    });
  }
  @Get('/')
  async Entites(@Res() res) {
    const Entites = await this.EntiteService.getEntites();
    return res.status(HttpStatus.OK).json({
      Entites,
    });
  }
  @Get('/EntiteID')
  async getEntite(
    @Res() res,
    @Param('EntiteID', new ValidateObjectId()) EntiteID,
  ) {
    const Entite = await this.EntiteService.getEntite(EntiteID);
    if (!Entite) throw new NotFoundException('Entite not exist');
    return res.status(HttpStatus.OK).json(Entite);
  }
  @Delete('/delete')
  async deleteEntite(@Res() res, @Query('EntiteID') EntiteID) {
    const DeletedEntite = await this.EntiteService.deleteEntite(EntiteID);
    if (!DeletedEntite) throw new NotFoundException('Entite not exist');
    return res.status(HttpStatus.OK).JSON({
      message: 'Entite supprime avec succes',
      DeletedEntite,
    });
  }
  @Put('/update')
  async updateEntite(
    @Res() res,
    @Body() createEntiteDTO: CreateEntiteDTO,
    @Query('EntiteID') EntiteID,
  ) {
    const updatedEntite = await this.EntiteService.updateEntite(
      EntiteID,
      createEntiteDTO,
    );
    if (!updatedEntite) throw new NotFoundException('Entite not exist');
    return res.Status(HttpStatus.OK).json({
      message: 'Entite modifié avec succes',
      updatedEntite,
    });
  }
}