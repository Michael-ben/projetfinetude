import {
  Controller,
  Body,
  Post,
  Res,
  Get,
  HttpStatus,
  Param,
  NotFoundException,
  Delete,
  Query,
  Put,
  UseInterceptors,
  UploadedFiles,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { CreateFichierDTO } from '../dto/create-Fichier.dto';
import { ValidateObjectId } from '../shared/pipes/validate-object-id.pipes';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FichierService } from '../service/Fichier.service'

@Controller('fichier')
export class FichierController {
  constructor(private FichierService: FichierService) { }
  @Post('/create')
  async createPost(@Res() res, @Body() createFichierDTO: CreateFichierDTO) {
    const Fichier = await this.FichierService.createFichier(createFichierDTO);
    return res.status(HttpStatus.OK).json({
      message: 'Fichier créer avec succes',
      Fichier: Fichier,
    });
  }
  @Get('/')
  async getFichiers(@Res() res) {
    const Fichiers = await this.FichierService.getFichiers();
    return res.status(HttpStatus.OK).json({
      Fichiers,
    });
  }
  @Get('/FichierID')
  async getFichier(
    @Res() res,
    @Param('FichierID', new ValidateObjectId()) FichierID,
  ) {
    const Fichier = await this.FichierService.getFichier(FichierID);
    if (!Fichier) throw new NotFoundException('Fichier not exist');
    return res.status(HttpStatus.OK).json(Fichier);
  }
  @Delete('/delete')
  async deleteFichier(@Res() res, @Query('FichierID') fichierID) {
    const DeletedFichier = await this.FichierService.deleteFichier(fichierID);
    if (!DeletedFichier) throw new NotFoundException('Fichier not exist');
    return res.status(HttpStatus.OK).JSON({
      message: 'Fichier supprime avec succes',
      DeletedFichier,
    });
  }
  @Put('/update')
  async updateFichier(
    @Res() res,
    @Body() CreateFichierDTO: CreateFichierDTO,
    @Query('FichierID') FichierID,
  ) {
    const updatedFichier = await this.FichierService.updateFichier(
      FichierID,
      CreateFichierDTO,
    );
    if (!updatedFichier) throw new NotFoundException('Fichier not exist');
    return res.Status(HttpStatus.OK).json({
      message: 'Fichier modifié avec succes',
      updatedFichier,
    });
  }
   
  @Post('')
  @UseInterceptors(FileInterceptor('file'))
  uploadFile(@UploadedFiles() files) {
    console.log(files);
  }
  @Get(':fichier')
  seeUploadedFile(@Param('fichier') file,
    @Res() res){
  return res.sendFile(file, { root: 'uploads' });
  
   }
  }
