import { Controller, Body, Post, Res, Get, HttpStatus, Param, NotFoundException, Delete, Query, Put } from '@nestjs/common';
import { CreateprofilDTO } from '../dto/create-profil.dto';
import { ProfilService } from '../service/profil.service';
import { ValidateObjectId } from '../shared/pipes/validate-object-id.pipes';


@Controller('profil')
export class ProfilController {
  constructor(private profilService: ProfilService) { }
  @Post('/create')
  async createPost(@Res() res, @Body() createprofilDTO: CreateprofilDTO) {
    const profil = await this.profilService.createprofils(createprofilDTO);
    return res.status
      (HttpStatus.OK).json({
        message: 'Acces créer avec succes',
        profil: profil,
      });
  }
  @Get('/')
  async getprofils(@Res() res) {
    const profils = await this.profilService.getprofils();
    return res.status(HttpStatus.OK).json({
      profils
    })
  }
  @Get('/profilID')
  async getAcces(@Res() res, @Param('profilID' ,new ValidateObjectId()) profilID) {
    const profil = await this.profilService.getprofil(profilID);
    if (!profil) throw new NotFoundException('profil not exist');
    return res.status(HttpStatus.OK).json(profil);
  }

  @Delete('/delete')
  async deleteAcces(@Res() res, @Query('profilID') profilID) {
    const Deletedprofil = await this.profilService.deleteprofil(profilID);
    if (!Deletedprofil) throw new NotFoundException('profil not exist');
    return res.status(HttpStatus.OK).JSON({
      message: 'profil supprime avec succes',
      Deletedprofil
    });
  }
  @Put('/update')
  async updateprofil(@Res() res, @Body() CreateprofilDTO: CreateprofilDTO, @Query('profilID') profilID) {
    const updatedprofil = await this.profilService.updateprofil(profilID, CreateprofilDTO);
    if (!updatedprofil) throw new NotFoundException('profil not exist');
    return res.Status(HttpStatus.OK).json({
      message: 'profil modifié avec succes',
      updatedprofil
    });

  }
}