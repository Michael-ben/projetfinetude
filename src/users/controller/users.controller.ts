import {
  UseGuards,
  Controller,
  Body,
  Post,
  Res,
  Get,
  HttpStatus,
  Param,
  NotFoundException,
  Delete,
  Query,
  Put,
} from '@nestjs/common';
import { RegisterparticulierDTO } from '../dto/registerparticulier.dto';
import { RegisterentrepriseDTO } from '../dto/registerentreprise.dto';
import { UsersService } from '../service/users.service';
import { AuthGuard } from '@nestjs/passport';

@Controller('user')
export class UsersController {
  constructor(private userService: UsersService) { }
  @Post('/createparticulier')
  async createparticulier(@Res() res, @Body() createuserDTO: RegisterparticulierDTO) {
    const user = await this.userService.createparticulier(createuserDTO);
    return res.status(HttpStatus.OK).json({
      message: 'user créer avec succes',
      user: user,
    });
  }
  @Post('/createentreprise')
  async createentreprise(@Res() res, @Body() createuserDTO: RegisterentrepriseDTO) {
    const user = await this.userService.createentreprise(createuserDTO);
    return res.status(HttpStatus.OK).json({
      message: 'user créer avec succes',
      user: user,
    });
  }
  @Get('/')
  async getUsers(@Res() res) {
    const users = await this.userService.getUsers();
    return res.status(HttpStatus.OK).json({
      users,
    });
  }
  @Get('/:userID')
  async getUser(@Res() res, @Param('userID') userID) {
    const user = await this.userService.getUser(userID);
    if (!user) throw new NotFoundException('user not exist');
    return res.status(HttpStatus.OK).json(user);
  }

  @Delete('/delete')
  async deleteUser(@Res() res, @Query('userID') userID) {
    const userDeleted = await this.userService.deleteUser(userID);
    if (!userDeleted) throw new NotFoundException('user not exist');
    return res.status(HttpStatus.OK).json({
      message: 'user supprime avec succes',
      userDeleted,
    });
  }
  @Put('/updateparticulier')
  async updateparticulier(
    @Res() res,
    @Body() createuserDTO: RegisterparticulierDTO,
    @Query('EntiteID') userID,
  ) {
    const updateduser = await this.userService.updateparticulier(
      userID,
      createuserDTO,
    );
    if (!updateduser) throw new NotFoundException('Entite not exist');
    return res.Status(HttpStatus.OK).json({
      message: 'user modifié avec succes',
      updateduser,
    });
  }
  @Put('/updateentreprise')
  async updateentreprise(
    @Res() res,
    @Body() createuserDTO: RegisterentrepriseDTO,
    @Query('EntiteID') userID,
  ) {
    const updateduser = await this.userService.updateentreprise(
      userID,
      createuserDTO,
    );
    if (!updateduser) throw new NotFoundException('Entite not exist');
    return res.Status(HttpStatus.OK).json({
      message: 'user modifié avec succes',
      updateduser,
    });
  }
  @Get('test')
  @UseGuards(AuthGuard())
  testAuthRoute() {
    return {
      message: 'You did it!',
    };
  }
}
