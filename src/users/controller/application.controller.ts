import { Controller, Body, Post, Res, Get, HttpStatus, Param, NotFoundException, Delete, Query, Put } from '@nestjs/common';
import { CreateapplicationDTO } from '../dto/create-application.dto';
import { ApplicationService } from '../service/application.service';
import { ValidateObjectId } from '../shared/pipes/validate-object-id.pipes';

@Controller('application')
export class ApplicationController {
  constructor(private applicationService: ApplicationService) {}
  @Post('/create')
  async createPost(
    @Res() res,
    @Body() createapplicationDTO: CreateapplicationDTO,
  ) {
    const application = await this.applicationService.createapplications(
      createapplicationDTO,
    );
    return res.status(HttpStatus.OK).json({
      message: 'Acces créer avec succes',
      application: application,
    });
  }
  @Get('/')
  async getapplications(@Res() res) {
    const application = await this.applicationService.getapplications();
    return res.status(HttpStatus.OK).json({
      application,
    });
  }
  @Get('/applicationID')
  async getapplication(
    @Res() res,
    @Param('applicationID', new ValidateObjectId()) applicationID,
  ) {
    const application = await this.applicationService.getapplication(
      applicationID,
    );
    if (!application) throw new NotFoundException('Acces not exist');
    return res.status(HttpStatus.OK).json(application);
  }
  @Delete('/delete')
  async deleteapplication(@Res() res, @Query('applicationID') applicationID) {
    const Deletedapplication = await this.applicationService.deleteapplication(
      applicationID,
    );
    if (!Deletedapplication)
      throw new NotFoundException('application not exist');
    return res.status(HttpStatus.OK).JSON({
      message: 'application supprime avec succes',
      Deletedapplication,
    });
  }
  @Put('/update')
  async updateapplication(
    @Res() res,
    @Body() CreateapplicationDTO: CreateapplicationDTO,
    @Query('applicationID') applicationID,
  ) {
    const updatedapplication = await this.applicationService.updateapplication(
      applicationID,
      CreateapplicationDTO,
    );
    if (!updatedapplication)
      throw new NotFoundException('application not exist');
    return res.Status(HttpStatus.OK).json({
      message: 'application modifié avec succes',
      updatedapplication,
    });
  }
}