import { Controller, Body, Post, Res, Get,HttpStatus, Param, NotFoundException, Delete , Query, Put} from '@nestjs/common';
import { CreateAccesDTO } from '../dto/create-Acces.dto';
import { ValidateObjectId } from '../shared/pipes/validate-object-id.pipes';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import{AccesService} from '../service/Acces.service'

@Controller('acces')
export class AccesController {
  constructor(private AccesService: AccesService) {}
  @Post('/create')
  async createPost(@Res() res, @Body() createAccesDTO: CreateAccesDTO) {
    const Acces = await this.AccesService.createAccess(createAccesDTO);
    return res.status(HttpStatus.OK).json({
      message: 'Acces créer avec succes',
      Acces: Acces,
    });
  }
  @Get('/')
  async getAccess(@Res() res) {
    const Access = await this.AccesService.getAccess();
    return res.status(HttpStatus.OK).json({
      Access,
    });
  }
  @Get('/AccesID')
  async getAcces(
    @Res() res,
    @Param('AccesID', new ValidateObjectId()) AccesID,
  ) {
    const Acces = await this.AccesService.getAcces(AccesID);
    if (!Acces) throw new NotFoundException('Acces not exist');
    return res.status(HttpStatus.OK).json(Acces);
  }
  @Delete('/delete')
  async deleteAcces(@Res() res, @Query('AccesID') AccesID) {
    const DeletedAcces = await this.AccesService.deleteAcces(AccesID);
    if (!DeletedAcces) throw new NotFoundException('Acces not exist');
    return res.status(HttpStatus.OK).JSON({
      message: 'Acces supprime avec succes',
      DeletedAcces,
    });
  }
  @Put('/update')
  async updateAcces(
    @Res() res,
    @Body() CreateAccesDTO: CreateAccesDTO,
    @Query('AccesID') AccesID,
  ) {
    const updatedAcces = await this.AccesService.updateAcces(
      AccesID,
      CreateAccesDTO,
    );
    if (!updatedAcces) throw new NotFoundException('Acces not exist');
    return res.Status(HttpStatus.OK).json({
      message: 'Acces modifié avec succes',
      updatedAcces,
    });
  }
}