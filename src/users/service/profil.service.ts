import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { profil } from '../interfaces/profil.interface';
import { CreateprofilDTO } from '../dto/create-profil.dto';
@Injectable()
export class ProfilService {
  profil: any;
  constructor(
    @InjectModel('Profil')
    private readonly profilModel: Model<profil>
  ) { }
  async createprofils(createprofilDTO: CreateprofilDTO): Promise<profil> {
    const profil = await new this.profilModel(createprofilDTO);
    return await profil.save();
  }
  async getprofils(): Promise<profil[]> {
    const profils = await this.profilModel.find();
    return profils;

  }
  async getprofil(profilID: string): Promise<profil> {
    const profil = await this.profilModel.findById(profilID);
    return profil;
  }


  async  updateprofil(profilID: string, createprofilDTO: CreateprofilDTO): Promise<profil> {
    const updatedprofil = await this.profilModel.findByIdAndUpdate(profilID, createprofilDTO, { new: true });
    return updatedprofil;
  }

  async deleteprofil(profilID: string): Promise<profil> {
    const deletedprofil = await this.profilModel.findByIdAndDelete(profilID);
    return deletedprofil;

  }
}