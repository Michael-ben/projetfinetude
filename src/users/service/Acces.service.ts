import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Acces } from '../interfaces/Acces.interface';
import { CreateAccesDTO } from '../dto/create-Acces.dto';
@Injectable()
export class AccesService {
  constructor(
    @InjectModel('Acces')
    private readonly AccesModel: Model<Acces>
  ) {}
  async createAccess(createAccesDTO: CreateAccesDTO): Promise<Acces> {
    const Acces = await new this.AccesModel(createAccesDTO);
    return await Acces.save();
    }
  async getAccess(): Promise<Acces []> {
    const Access = await this.AccesModel.find();
    return Access;
    
  }
  async getAcces(AccesID: string): Promise<Acces>{
    const Acces = await this.AccesModel.findById(AccesID);
    return Acces;
  }


 async  updateAcces(AccesID: string, createAccesDTO: CreateAccesDTO): Promise<Acces> {
   const updatedAcces = await this.AccesModel.findByIdAndUpdate(AccesID, createAccesDTO,{new : true});
   return updatedAcces;
  }

  async deleteAcces(AccesID: string): Promise<Acces> {
    const deletedAcces = await this.AccesModel.findByIdAndDelete(AccesID);
    return deletedAcces;
    
  }
}