import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { application } from '../interfaces/application.interface';
import { CreateapplicationDTO } from '../dto/create-application.dto';
import { InjectModel } from '@nestjs/mongoose';
@Injectable()
export class ApplicationService {
  constructor(
    @InjectModel('Application')
    private readonly applicationModel: Model<application>,
  ) {}
  async createapplications(
    createapplicationDTO: CreateapplicationDTO,
  ): Promise<application> {
    const application = await new this.applicationModel(createapplicationDTO);
    return await application.save();
  }
  async getapplications(): Promise<application[]> {
    const applications = await this.applicationModel.find();
    return applications;
  }
  async getapplication(applicationID: string): Promise<application> {
    const application = await this.applicationModel.findById(applicationID);
    return application;
  }

  async updateapplication(
    applicationID: string,
    createapplicationDTO: CreateapplicationDTO,
  ): Promise<application> {
    const updatedapplication = await this.applicationModel.findByIdAndUpdate(
      applicationID,
      createapplicationDTO,
      { new: true },
    );
    return updatedapplication;
  }

  async deleteapplication(applicationID: string): Promise<application> {
    const deletedapplication = await this.applicationModel.findByIdAndDelete(
      applicationID,
    );
    return deletedapplication;
  }
}
