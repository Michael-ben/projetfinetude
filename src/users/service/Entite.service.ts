import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Entite } from '../interfaces/Entite.interface';
import { CreateEntiteDTO } from '../dto/create-Entite.dto';
@Injectable()
export class EntiteService {
  entite: any;
  constructor(
    @InjectModel('Entite')
    private readonly EntiteModel: Model<Entite>
  ) {}
  async createEntites(createEntiteDTO: CreateEntiteDTO): Promise<Entite> {
    const Entite = await new this.EntiteModel(createEntiteDTO);
    return await Entite.save();
  }
  async getEntites(): Promise<Entite[]> {
    const Entites = await this.EntiteModel.find();
    return Entites;

  }
  async getEntite(EntiteID: string): Promise<Entite> {
    const Entite = await this.EntiteModel.findById(EntiteID);
    return Entite;
  }


  async  updateEntite(EntiteID: string, createEntiteDTO: CreateEntiteDTO): Promise<Entite> {
    const updatedEntite = await this.EntiteModel.findByIdAndUpdate(EntiteID, createEntiteDTO, { new: true });
    return updatedEntite;
  }

  async deleteEntite(EntiteID: string): Promise<Entite> {
    const deletedEntite = await this.EntiteModel.findByIdAndDelete(EntiteID);
    return deletedEntite;

  }
  async findOneByEntite(entite): Promise<Entite> {

    return await this.EntiteModel.findOne({ entite: entite });
  }
}