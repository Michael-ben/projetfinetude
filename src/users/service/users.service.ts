/* eslint-disable @typescript-eslint/camelcase */
import { Injectable,HttpException,HttpStatus } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { User } from '../interfaces/user.interface';
import { RegisterentrepriseDTO } from '../dto/registerentreprise.dto';
import { RegisterparticulierDTO } from '../dto/registerparticulier.dto';
@Injectable()
export class UsersService {
  constructor(
    @InjectModel('User')
    private userModel: Model<User>,
  ) { }
  async createparticulier(createuserDTO: RegisterparticulierDTO): Promise<User> {
    const { e_mail, entite } = createuserDTO;
    const user = await this.userModel.findOne({ e_mail });
    const users = await this.userModel.findOne({ entite });
    if (user) {
      throw new HttpException('user already exists', HttpStatus.BAD_REQUEST);
    }
    if (users) {
      throw new HttpException('user already exists', HttpStatus.BAD_REQUEST);
    }
    const createduser = new this.userModel(createuserDTO);
    return await createduser.save();
  }
  async createentreprise(createuserDTO: RegisterentrepriseDTO): Promise<User> {
    const { e_mail, raison_sociale } = createuserDTO;
    const user = await this.userModel.findOne({ e_mail });
    const users = await this.userModel.findOne({ raison_sociale });
    if (user) {
      throw new HttpException('user already exists', HttpStatus.BAD_REQUEST);
    }
    if (users) {
      throw new HttpException('user already exists', HttpStatus.BAD_REQUEST);
    }
    const createduser = new this.userModel(createuserDTO);
    return await createduser.save();
  }

  async getUsers(): Promise<User[]> {
    const users = await this.userModel.find();
    return users;
  }
  async getUser(userID: string): Promise<User> {
    const user = await this.userModel.findById(userID);
    return user;
  }

  async updateparticulier(
    userID: string,
    createuserDTO: RegisterparticulierDTO,
  ): Promise<User> {
    const updatedUser = await this.userModel.findByIdAndUpdate(
      userID,
      createuserDTO,
      { new: true },
    );
    return updatedUser;
  }
  async updateentreprise(
    userID: string,
    createuserDTO: RegisterentrepriseDTO,
  ): Promise<User> {
    const updatedUser = await this.userModel.findByIdAndUpdate(
      userID,
      createuserDTO,
      { new: true },
    );
    return updatedUser;
  }

  async deleteUser(userID: string): Promise<User> {
    const deletedUser = await this.userModel.findByIdAndDelete(userID);
    return deletedUser;
  }
  async findOneByEntite(entite): Promise<User> {
    return await this.userModel.findOne({ entite: entite });
  }
  async findOneByUsername(username): Promise<User> {
    return await this.userModel.findOne({ username: username });
  }
  async findOneByPassword(password): Promise<User> {
    return await this.userModel.findOne({ password: password });
}
}
