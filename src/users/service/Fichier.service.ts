import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Fichier} from '../interfaces/Fichier.interface';
import { CreateFichierDTO } from '../dto/create-Fichier.dto';
@Injectable()
export class FichierService {
  Fichier: any;
  constructor(
    @InjectModel('Fichier')
    private readonly fichierModel: Model<Fichier>
  ) { }
  async createFichier(createFichierDTO: CreateFichierDTO): Promise<Fichier> {
    const fichier = await new this.fichierModel(createFichierDTO);
    return await fichier.save();
  }
  async getFichiers(): Promise<Fichier[]> {
    const fichiers = await this.fichierModel.find();
    return fichiers;

  }
  async getFichier(fichierID: string): Promise<Fichier> {
    const fichier = await this.fichierModel.findById(fichierID);
    return fichier;
  }


  async  updateFichier(fichierID: string, createFichierDTO: CreateFichierDTO): Promise<Fichier> {
    const updatedFichier = await this.fichierModel.findByIdAndUpdate(fichierID, createFichierDTO, { new: true });
    return updatedFichier;
  }

  async deleteFichier(fichierID: string): Promise<Fichier> {
    const deletedFichier = await this.fichierModel.findByIdAndDelete(fichierID);
    return deletedFichier;

  }
}