/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-this-alias */
/* eslint-disable @typescript-eslint/camelcase */
import * as mongoose from 'mongoose';
import * as bcrypt from 'bcrypt';
import { User } from '../interfaces/user.interface';
import { model } from 'mongoose';
export const UserSchema = new mongoose.Schema({
  
  nom: {
    type: String,
    trim: true,
  },
  prenom: {
    type: String,
    trim: true,
  },
  e_mail: {
    type: String,
    trim: true,
    unique: true,
  },
  username: {
    type: String,
    trim: true,
    unique:true,
  },
  raison_sociale: {
    type: String,
    trim: true,
    unique:true,
  },
  entite: {
    type: String,
    trim: true,
    unique:true,
  },
  ville: {
    type: String,
    trim: true,
    default: null,
  },
  pays: {
    type: String,
    trim: true,
    default: null,
  },
  numero_tel:Number,
  password:String,
  createdat: {
    type: Date,
    default: Date.now,
  },
  updatedat: Date,
  createdby: String,
  uptdatedby: String
});
// eslint-disable-next-line @typescript-eslint/no-unused-vars
model('user', UserSchema);
UserSchema.pre<User>("save", function(next) {

  const user = this;

  if (!user.isModified('password')) return next();

 
  bcrypt.genSalt(10, (err, salt) => {

    if (err){
      return next(err);
    }
    bcrypt.hash(user.password, salt, (err:mongoose.Error, hash) => {
      if (err) {
        return next(err);
         }
      user.password = hash;
      next();

    });
    
  });

});

UserSchema.methods.checkPassword = function (attempt, callback) {

  const user = this;

  bcrypt.compare(attempt, user.password, (err, isMatch) => {
    if (err) return callback(err);
    callback(null, isMatch);
  });

};
