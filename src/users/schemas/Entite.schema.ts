/* eslint-disable @typescript-eslint/camelcase */
import * as mongoose from 'mongoose';
import { model } from 'mongoose';
export const EntiteSchema = new mongoose.Schema({
  entite: {
    type: String,
    unique: true,
    trim: true,
    default: null,
  },
  created_at: { type: Date },
  uptdated_at: { type: Date },
  created_by: { type: String },
  uptdated_by: { type: String },
  user: {
    type: mongoose.Schema.Types.ObjectId, ref: 'user'
  },
  
});
model('Entite', EntiteSchema);
