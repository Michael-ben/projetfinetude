/* eslint-disable @typescript-eslint/camelcase */
import * as  mongoose from 'mongoose';

export const ProfilSchema = new mongoose.Schema({
  nom_profil: {
    type: String,
    unique: true,
    trim: true,
    default: null,
  },
  status: {
    type: String,
    unique: true,
    trim: true,
    default: null,
  },
  created_at: Date,
  updated_at: Date ,
  created_by: String ,
  uptdated_by:  String ,
  user: {
    type :mongoose.Schema.Types.ObjectId, ref :'User'
  },
  entite: {
    type : mongoose.Schema.Types.ObjectId , ref : 'Entite'
    }
});

