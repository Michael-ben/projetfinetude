/* eslint-disable @typescript-eslint/camelcase */
import { Schema, model } from 'mongoose';
export const FichierSchema = new Schema({
  nom_fichier: {
    type: String,
    unique: true,
    trim: true,
    default: null,
  },
  type_fichier: {
    type: String
  },
  taille_fichier: { type: String },
  format_fichier: { type: String },
  created_at: {type : Date} ,
  uptdated_at: { type: Date },
  created_by: { type :String} ,
  uptdated_by: { type: String },
});
 model('Fichier', FichierSchema);
