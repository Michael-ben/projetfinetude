/* eslint-disable @typescript-eslint/camelcase */
import { Schema, model } from 'mongoose';
export const AccesSchema = new Schema({
  nom_acces: {
    type: String,
    unique: true,
    trim: true,
    default: null,
  }, 
  created_at: { type: Date },
  updated_at: { type: Date } ,
  created_by:{type : String} ,
  uptdated_by: { type: String },
});
 model('Acces', AccesSchema);
