/* eslint-disable @typescript-eslint/camelcase */
import { Schema, model } from 'mongoose';
export const ApplicationSchema = new Schema({
  nomapp: {
    type: String,
    unique: true,
    trim: true,
    default: null,
  },
  cleapp: {
    type: String,
    unique: true,
    trim: true,
    default: null,
  },
  created_at: { type: Date},
  uptdated_at: { type: Date },
  created_by: { type: String },
  uptdated_by: {
    type: String
  }
})
model('application', ApplicationSchema);
