export class CreateprofilDTO {
  readonly id_profil: number;
  readonly nom_profil: string;
  readonly status: string;
  readonly created_at: Date;
  readonly updated_at: Date;
  readonly created_by: string;
  readonly uptdated_by: string;
}
