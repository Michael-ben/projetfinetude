export class CreateFichierDTO {
  
  readonly nom_fichier: string;
  readonly type_fichier: string;
  readonly taille_fichier: string;
  readonly format_fichier: string;
  readonly created_at: Date;
  readonly updated_at: Date;
  readonly created_by: string;
  readonly uptdated_by: string;
}
