export class RegisterentrepriseDTO {
  readonly nom: string;
  readonly prenoms: string;
  readonly e_mail: string;
  readonly username: string;
  readonly entite: string;
  readonly raison_sociale: string;
  readonly ville: string;
  readonly numero_tel: string;
  readonly pays: string;
  readonly password: string;
}