export class CreateAccesDTO {
  readonly idAcces: number;
  readonly nomAcces: string;
  readonly createdat: Date;
  readonly updatedat: Date;
  readonly createdby: string;
  readonly uptdatedby: string;
}
