export class LoginDTO {
  readonly entite: string;
  readonly raison_sociale: string;
  readonly username: string;
  readonly password: string;
}