export class RegisterparticulierDTO {
  readonly nom: string;
  readonly prenoms: string;
  readonly e_mail: string;
  readonly username: string;
  readonly ville: string;
  readonly entite: string;
  readonly numero_tel: number;
  readonly pays: string;
  readonly password: string;
}
