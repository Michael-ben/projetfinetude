export class CreateEntiteDTO {
  readonly domaine: string;
  readonly created_at: Date;
  readonly updated_at: Date;
  readonly created_by: string;
  readonly uptdated_by: string;
}
